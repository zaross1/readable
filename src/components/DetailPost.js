import React from "react";
import NavBar from "./NavBar.js";
import Comment from "./Comment.js";
import sortBy from "sort-by";
import { deletePost } from "../actions/post.js";
import { updateSelectedPost } from "../actions/selectedPost.js";
import { connect } from "react-redux";
import { withRouter, Link } from "react-router-dom";
import { deletePostDb } from "../utils/helpers.js";
import { updatePost } from "../actions/post.js";
import * as helpers from "../utils/helpers.js";

class DetailPost extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      sortingMethod: "-timestamp"
    };
  }

  vote = direction => {
    console.log(this.props.store.selectedPost);
    let votedPost = { ...this.props.store.selectedPost };

    if (direction === "upVote") {
      votedPost["voteScore"] += 1;
    } else votedPost["voteScore"] -= 1;

    helpers.voteDb(
      this.props,
      votedPost,
      direction,
      `http://localhost:3001/posts/${votedPost.id}`
    );
  };

  render() {
    return (
      <div>
        <NavBar />

        <div className="detail-post">
          <Link style={{ marginRight: "5px" }} to="/editPost">
            Edit
          </Link>
          <Link
            style={{ marginRight: "5px" }}
            to="/home"
            onClick={() =>
              deletePostDb(this.props, this.props.store.selectedPost.id)
            }
          >
            Delete
          </Link>
          <Link style={{ marginRight: "5px" }} to="/newComment">
            New Comment
          </Link>
          <label>
            VoteScore: {this.props.store.selectedPost.voteScore}
            <button onClick={() => this.vote("upVote")}>Upvote</button>
            <button onClick={() => this.vote("downVote")}>Downvote</button>
          </label>

          <h1>{this.props.store.selectedPost.title}</h1>
          <h2>Author: {this.props.store.selectedPost.author}</h2>
          <h2>Category: {this.props.store.selectedPost.category}</h2>
          <br />

          {this.props.store.selectedPost.body}
        </div>

        <h2>Comments</h2>

        <div>
          {helpers
            .objToArr(this.props.store.comments)
            .filter(
              comment =>
                comment.deleted !== true &&
                comment.parentId === this.props.store.selectedPost.id
            )
            .sort(sortBy(this.state.sortingMethod))
            .map(comment => <Comment key={comment.id} info={comment} />)}
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    store: state
  };
};

const mapDispatchToProps = dispatch => {
  return {
    changeSelectedPost: newValue => dispatch(updateSelectedPost(newValue)),
    updatePost: newValue => dispatch(updatePost(newValue)),
    deletePost: id => dispatch(deletePost(id))
  };
};

export default withRouter(
  connect(mapStateToProps, mapDispatchToProps, null, { pure: false })(
    DetailPost
  )
);
