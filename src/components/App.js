import React, { Component } from "react";
import "../App.css";
import {Route, Switch, withRouter } from "react-router-dom";
import Home from "./Home.js";
import Categories from "./Categories.js";
import UpdatePost from "./UpdatePost.js";
import UpdateComment from "./UpdateComment.js";
import DetailPost from "./DetailPost.js";
import { connect } from "react-redux";
import { setPosts } from "../actions/post.js";
import { updateSelectedPost } from "../actions/selectedPost.js";

class App extends Component {
  componentWillMount() {
    //this.props.changeSelectedPost(null)
  }

  constructor(props) {
    super(props);

    this.state = {};
  }

  render() {
    return (
      <div className="App">
        <Switch>
          <Route exact path="/" render={() => <Home />} /> )}
          <Route path="/home" render={() => <Home />} /> )}
          <Route path="/categories" render={() => <Categories />} />
          <Route path="/newPost" render={() => <UpdatePost />} />
          <Route path="/newComment" render={() => <UpdateComment />} />
          <Route
            path="/editPost"
            render={() => <UpdatePost post={this.props.store.selectedPost} />}
          />
          <Route
            path="/editComment"
            render={() => (
              <UpdateComment comment={this.props.store.selectedComment} />
            )}
          />
          {this.props.store.selectedPost !== null && (
            <Route
              path={`/posts/${this.props.store.selectedPost.title}`}
              render={() => <DetailPost />}
            />
          )}
        </Switch>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    store: state
  };
};

const mapDispatchToProps = dispatch => {
  return {
    setAllPosts: posts => dispatch(setPosts(posts)),
    changeSelectedPost: newValue => dispatch(updateSelectedPost(newValue))
  };
};

export default withRouter(
  connect(mapStateToProps, mapDispatchToProps, null, { pure: false })(App)
);
