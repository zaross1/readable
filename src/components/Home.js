import React from "react";
import Flexbox from "flexbox-react";
import NavBar from "./NavBar.js";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import SimplePost from "./SimplePost.js";
import * as helpers from "../utils/helpers.js";
import sortBy from "sort-by";
import { setPosts } from "../actions/post.js";
import { updateSelectedPost } from "../actions/selectedPost.js";
import { updateSelectedComment } from "../actions/selectedComment.js";

class Home extends React.Component {
  componentDidMount() {
    this.props.changeSelectedComment(
      this.props.store.comments["894tuq4ut84ut8v4t8wun89g"]
    );

    const postUrl = "http://localhost:3001/posts";
    const stuff = {
      method: "GET",
      credentials: "omit",
      headers: new Headers({ Authorization: "something" })
    };

    fetch(postUrl, stuff)
      .then(data => data.json())
      .then(data => this.props.setAllPosts(helpers.arrToObj(data)));
  }

  constructor(props) {
    super(props);

    this.state = {
      sortingMethod: "voteScore"
    };
  }

  render() {
    return (
      <Flexbox flexDirection="column" minHeight="100vh">
        <Flexbox element="header" height="80px" width="100%">
          <Flexbox flexDirection="row" width="100%">
            <NavBar />
          </Flexbox>
        </Flexbox>

        <Flexbox flexDirection="row" width="100%">
          <Flexbox
            onClick={() => this.setState({ sortingMethod: "title" })}
            flexDirection="column"
            width="30%"
          >
            <label className="category-label">Title</label>
          </Flexbox>

          <Flexbox
            onClick={() => this.setState({ sortingMethod: "category" })}
            flexDirection="column"
            width="15%"
          >
            <label className="category-label">Category</label>
          </Flexbox>

          <Flexbox
            onClick={() => this.setState({ sortingMethod: "-voteScore" })}
            flexDirection="column"
            width="15%"
          >
            <label className="category-label">Vote Score</label>
          </Flexbox>

          <Flexbox
            onClick={() => this.setState({ sortingMethod: "-commentCount" })}
            flexDirection="column"
            width="15%"
          >
            <label className="category-label">Comments</label>
          </Flexbox>

          <Flexbox
            onClick={() => this.setState({ sortingMethod: "-timeStamp" })}
            flexDirection="column"
            width="15%"
          >
            <label className="category-label">Time</label>
          </Flexbox>
        </Flexbox>

        {helpers
          .objToArr(this.props.store.posts)
          .filter(post => post.deleted !== true)
          .sort(sortBy(this.state.sortingMethod))
          .map(post => (
            <SimplePost showCategory={true} key={post.id} info={post} />
          ))}
      </Flexbox>
    );
  }
}

const mapStateToProps = state => {
  return {
    store: state
  };
};

const mapDispatchToProps = dispatch => {
  return {
    setAllPosts: posts => dispatch(setPosts(posts)),
    changeSelectedPost: newValue => dispatch(updateSelectedPost(newValue)),
    changeSelectedComment: comment => dispatch(updateSelectedComment(comment))
  };
};

export default withRouter(
  connect(mapStateToProps, mapDispatchToProps, null, { pure: false })(Home)
);
