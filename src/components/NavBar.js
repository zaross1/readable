import React from "react";
import { Link } from "react-router-dom";

const NavBar = props => {
  return (
    <div className="nav-bar">
      <ul className="nav-list">
        <li className="nav-list-item">
          <Link style={{ textDecoration: "none" }} to="/home">
            Home
          </Link>
        </li>
        <li className="nav-list-item">
          <Link style={{ textDecoration: "none" }} to="/categories">
            Categories
          </Link>
        </li>
        <li className="nav-list-item">
          <Link style={{ textDecoration: "none" }} to="/newPost">
            New Post
          </Link>
        </li>
      </ul>
    </div>
  );
};

export default NavBar;
