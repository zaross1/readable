import React from "react";
import Flexbox from "flexbox-react";
import { Link, withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { updateSelectedPost } from "../actions/selectedPost.js";

class SimplePost extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      showCategory: props.showCategory,
      time: new Date(props.info.timestamp)
    };
  }

  render() {
    return (
      <Flexbox flexDirection="row" width="100%">
        <Flexbox flexDirection="column" width="30%">
          <label
            className="post-label"
            onClick={() => this.props.changeSelectedPost(this.props.info)}
          >
            <Link to={`posts/${this.props.info.title}`}>
              {this.props.info.title}
            </Link>
          </label>
        </Flexbox>

        {this.state.showCategory && (
          <Flexbox flexDirection="column" width="15%">
            <label className="post-label">{this.props.info.category}</label>
          </Flexbox>
        )}

        <Flexbox flexDirection="column" width="15%">
          <label className="post-label">{this.props.info.voteScore}</label>
        </Flexbox>

        <Flexbox flexDirection="column" width="15%">
          <label className="post-label">{this.props.info.commentCount}</label>
        </Flexbox>

        <Flexbox flexDirection="column" width="15%">
          <label className="post-label">{this.state.time.toDateString()}</label>
        </Flexbox>
      </Flexbox>
    );
  }
}

const mapStateToProps = state => {
  return {
    store: state
  };
};

const mapDispatchToProps = dispatch => {
  return {
    changeSelectedPost: newValue => dispatch(updateSelectedPost(newValue))
  };
};

export default withRouter(
  connect(mapStateToProps, mapDispatchToProps, null, { pure: false })(
    SimplePost
  )
);
