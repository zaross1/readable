import React from "react";
import Flexbox from "flexbox-react";
import NavBar from "./NavBar.js";
import { connect } from "react-redux";
import { withRouter, Redirect } from "react-router-dom";
import * as helpers from "../utils/helpers.js";
import { updatePost, createPost } from "../actions/post.js";
import { Form, Text, TextArea, Radio, RadioGroup } from "react-form";
const uniqueId = require("uniqid");

class UpdatePost extends React.Component {
  componentDidMount() {
    this.setState({ submitted: false });
  }

  createPost = postData => {
    const time = new Date();

    if (this.props.post === undefined) {
      this.setState(state => {
        return {
          ...state,
          post: {
            ...state["post"],
            title: postData.title,
            body: postData.body,
            author: postData.author,
            category: postData.category,
            timestamp: time.getTime()
          }
        };
      });

      helpers.createPostDb(this.props, this.state.post);
    } else {
      const editedPost = {
        id: this.props.post.id,
        title:
          postData.title !== undefined ? postData.title : this.props.post.title,
        body:
          postData.body !== undefined ? postData.body : this.props.post.body,
        author: this.props.post.author,
        category: this.props.post.category,
        timestamp: time.getTime(),
        commentCount: this.props.post.commentCount,
        deleted: this.props.post.deleted,
        voteScore: this.props.post.voteScore
      };

      helpers.updatePostDb(this.props, editedPost);
    }

    this.setState({ submitted: true });
  };

  constructor(props) {
    super(props);

    if (props.post !== undefined) {
      this.state = {
        post: props.post
      };
    } else {
      this.state = {
        submitted: false,
        post: {
          id: uniqueId(),
          timestamp: null,
          title: null,
          body: null,
          author: null,
          category: null,
          voteScore: 0,
          deleted: false,
          commentCount: 0
        }
      };
    }
  }

  render() {
    return (
      <Flexbox flexDirection="column" minHeight="100vh">
        <Flexbox element="header" height="80px" width="100%">
          <Flexbox flexDirection="row" width="100%">
            <NavBar />
          </Flexbox>
        </Flexbox>

        <Flexbox flexDirection="row" width="100%" />

        <Form onSubmit={this.createPost}>
          {formApi => (
            <form onSubmit={formApi.submitForm} id="form1">
              <label htmFor="title">Post Title</label>
              <Text
                field="title"
                placeholder={
                  this.props.post !== undefined ? this.props.post.title : ""
                }
                className="form-field"
                id="title"
              />
              <br />
              {this.props.post === undefined && (
                <div>
                  <label htmFor="author">Author</label>
                  <Text
                    field="author"
                    placeholder={
                      this.props.post !== undefined
                        ? this.props.post.author
                        : ""
                    }
                    className="form-field"
                    id="author"
                  />
                  <br />
                </div>
              )}
              {this.props.post === undefined && (
                <RadioGroup field="category">
                  {category => (
                    <div>
                      <label htmlFor="react" className="radio-item-label">
                        React
                      </label>
                      <Radio
                        group={category}
                        value="react"
                        id="react"
                        className="radio-item"
                      />
                      <label htmlFor="redux" className="radio-item-label">
                        Redux
                      </label>
                      <Radio
                        group={category}
                        value="redux"
                        id="redux"
                        className="radio-item"
                      />
                      <label htmlFor="udacity" className="radio-item-label">
                        Udacity
                      </label>
                      <Radio
                        group={category}
                        value="udacity"
                        id="redux"
                        className="radio-item"
                      />
                    </div>
                  )}
                </RadioGroup>
              )}

              <label htmlFor="body">Body</label>
              <br />
              <TextArea
                style={{ height: "200px" }}
                field="body"
                placeholder={
                  this.props.post !== undefined ? this.props.post.body : ""
                }
                id="body"
              >
                {this.props.post !== undefined && this.props.post.body}
              </TextArea>
              <br />

              <button type="submit">Submit Post</button>
            </form>
          )}
        </Form>
        {this.state.submitted && <Redirect to="/home" />}
      </Flexbox>
    );
  }
}

const mapStateToProps = state => {
  return {
    store: state
  };
};

const mapDispatchToProps = dispatch => {
  return {
    createPost: post => dispatch(createPost(post)),
    updatePost: post => dispatch(updatePost(post))
  };
};

export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(UpdatePost)
);
