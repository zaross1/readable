import React from "react";
import { Form, Text, TextArea } from "react-form";
import { connect } from "react-redux";
import Flexbox from "flexbox-react";
import { withRouter, Redirect } from "react-router-dom";
import NavBar from "./NavBar.js";
import * as helpers from "../utils/helpers.js";
import { createComment, updateComment } from "../actions/comment.js";
const uniqueId = require("uniqid");

class UpdateComment extends React.Component {
  componentDidMount() {
    console.log(this.props.comment);
  }

  newComment = commentData => {
    const time = new Date();

    if (this.props.comment === undefined) {
      this.setState(state => {
        return {
          ...state,
          comment: {
            ...state["comment"],
            title: commentData.title,
            body: commentData.body,
            author: commentData.author,
            timestamp: time.getTime()
          }
        };
      });

      helpers.createCommentDb(this.props, this.state.comment);
    } else {
      const editedComment = {
        id: this.props.comment.id,
        title:
          commentData.title !== undefined
            ? commentData.title
            : this.props.comment.title,
        body:
          commentData.body !== undefined
            ? commentData.body
            : this.props.comment.body,
        author: this.props.comment.author,
        timestamp: time.getTime(),
        parentId: this.props.comment.parentId,
        deleted: this.props.comment.deleted,
        voteScore: this.props.comment.voteScore
      };

      helpers.updateCommentDb(this.props, editedComment);
    }

    this.setState({ submitted: true });
  };

  constructor(props) {
    super(props);

    if (props.comment === undefined) {
      this.state = {
        submitted: false,
        comment: {
          id: uniqueId(),
          parentId: props.store.selectedPost.id,
          timestamp: null,
          body: null,
          author: null,
          voteScore: 0,
          deleted: false,
          parentDeleted: false
        }
      };
    } else {
      this.state = {
        submitted: false,
        comment: props.comment
      };
    }
  }

  render() {
    return (
      <Flexbox flexDirection="column" minHeight="100vh">
        <Flexbox element="header" height="80px" width="100%">
          <Flexbox flexDirection="row" width="100%">
            <NavBar />
          </Flexbox>
        </Flexbox>

        <Flexbox flexDirection="row" width="100%" />

        <Form onSubmit={this.newComment}>
          {formApi => (
            <form onSubmit={formApi.submitForm} id="form1">
              <label htmFor="title">Title</label>
              <Text
                field="title"
                placeholder={
                  this.props.comment !== undefined
                    ? this.props.comment.title
                    : ""
                }
                className="form-field"
                id="title"
              />
              <br />
              {this.props.comment === undefined && (
                <div>
                  <label htmFor="author">Author</label>
                  <Text
                    field="author"
                    placeholder={
                      this.props.comment !== undefined
                        ? this.props.comment.author
                        : ""
                    }
                    className="form-field"
                    id="author"
                  />
                  <br />
                </div>
              )}

              <label htmlFor="body">Body</label>
              <br />
              <TextArea
                style={{ height: "200px" }}
                field="body"
                placeholder={
                  this.props.comment !== undefined
                    ? this.props.comment.body
                    : ""
                }
                id="body"
              >
                {this.props.comment !== undefined && this.props.comment}
              </TextArea>
              <br />

              <button type="submit">Submit Comment</button>
            </form>
          )}
        </Form>
        {this.state.submitted && (
          <Redirect to={`/posts/${this.props.store.selectedPost.title}`} />
        )}
      </Flexbox>
    );
  }
}

const mapStateToProps = state => {
  return {
    store: state
  };
};

const mapDispatchToProps = dispatch => {
  return {
    createComment: comment => dispatch(createComment(comment)),
    updateComment: comment => dispatch(updateComment(comment))
  };
};

export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(UpdateComment)
);
