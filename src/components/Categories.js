import React from "react";
import Flexbox from "flexbox-react";
import NavBar from "./NavBar.js";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import SimplePost from "./SimplePost.js";
import * as helpers from "../utils/helpers.js";
import sortBy from "sort-by";
import Dropdown from "react-dropdown";
import * as icons from "react-icons/lib/ti";

class Categories extends React.Component {
  componentDidMount() {
    console.log(this.props.store.categories);
  }

  selectCategory(category) {
    this.setState({ category: category.value });
    console.log(this.state.category);
  }

  constructor(props) {
    super(props);

    const categories = props.store.categories;
    let options = [];

    for (let i = 0; i < categories.length; i++) {
      options.push({ value: categories[i], label: categories[i] });
    }

    this.state = {
      sortingMethod: "-timeStamp",
      category: "react",
      options
    };

    this.selectCategory = this.selectCategory.bind(this);
  }

  render() {
    return (
      <Flexbox flexDirection="column" minHeight="100vh">
        <Flexbox element="header" height="80px" width="100%">
          <Flexbox flexDirection="row" width="100%">
            <NavBar />
          </Flexbox>
        </Flexbox>

        <Flexbox flexDirection="row" width="100%">
          <Flexbox
            onClick={() => this.setState({ sortingMethod: "title" })}
            flexDirection="column"
            width="30%"
          >
            <label className="category-label">Title</label>
          </Flexbox>

          <Flexbox
            onClick={() => this.setState({ sortingMethod: "-voteScore" })}
            flexDirection="column"
            width="15%"
          >
            <label className="category-label">Vote Score</label>
          </Flexbox>

          <Flexbox
            onClick={() => this.setState({ sortingMethod: "-commentCount" })}
            flexDirection="column"
            width="15%"
          >
            <label className="category-label">Comments</label>
          </Flexbox>

          <Flexbox
            onClick={() => this.setState({ sortingMethod: "-timeStamp" })}
            flexDirection="column"
            width="15%"
          >
            <label className="category-label">Time</label>
          </Flexbox>

          <Flexbox flexDirection="column">
            <div className="drop-down">
              <label>Category{<icons.TiArrowDown />}</label>
              <Dropdown
                className="drop-down-box"
                options={this.state.options}
                onChange={this.selectCategory}
                value={this.state.options[0]}
                placeholder={"Choose a category"}
              />
            </div>
          </Flexbox>
        </Flexbox>

        {helpers
          .objToArr(this.props.store.posts)
          .filter(post => post.category === this.state.category)
          .sort(sortBy(this.state.sortingMethod))
          .map(post => (
            <SimplePost showCategory={false} key={post.id} info={post} />
          ))}
      </Flexbox>
    );
  }
}

const mapStateToProps = state => {
  return {
    store: state
  };
};
export default withRouter(connect(mapStateToProps)(Categories));
