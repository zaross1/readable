import React from "react";
import { deleteComment } from "../actions/comment.js";
import { updateSelectedComment } from "../actions/selectedComment.js";
import { updateComment } from "../actions/comment.js";
import { connect } from "react-redux";
import { withRouter, Link } from "react-router-dom";
import { deleteCommentDb} from "../utils/helpers.js";
import * as helpers from "../utils/helpers.js";

class Comment extends React.Component {
  vote = direction => {
    console.log("fired");
    let votedComment = { ...this.props.info };

    if (direction === "upVote") {
      votedComment["voteScore"] += 1;
    } else votedComment["voteScore"] -= 1;

    helpers.voteDb(
      this.props,
      votedComment,
      direction,
      `http://localhost:3001/comments/${votedComment.id}`
    );
  };

  render() {
    return (
      <div className="comment">
        <Link
          onClick={() => this.props.changeSelectedComment(this.props.info)}
          style={{ marginRight: "5px" }}
          to="/editComment"
        >
          Edit
        </Link>
        <label
          className="delete-label"
          onClick={() => deleteCommentDb(this.props, this.props.info.id)}
        >
          Delete
        </label>
        <label>
          VoteScore: {this.props.info.voteScore}
          <button onClick={() => this.vote("upVote")}>Upvote</button>
          <button onClick={() => this.vote("downVote")}>Downvote</button>
        </label>
        <h3>{this.props.info.title}</h3>
        <h4>Author: {this.props.info.author}</h4>
        <p>{this.props.info.body}</p>
      </div>
    );
  }
}
const mapStateToProps = state => {
  return {
    store: state
  };
};

const mapDispatchToProps = dispatch => {
  return {
    changeSelectedComment: newValue =>
      dispatch(updateSelectedComment(newValue)),
    deleteComment: id => dispatch(deleteComment(id)),
    updateComment: newValue => dispatch(updateComment(newValue))
  };
};

export default withRouter(
  connect(mapStateToProps, mapDispatchToProps, null, { pure: false })(Comment)
);
