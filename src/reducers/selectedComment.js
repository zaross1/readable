import { store } from "../store.js";
import { UPDATE_SELECTED_COMMENT } from "../actions/selectedComment.js";

export function selectedCommentReducer(state = store.selectedComment, action) {
  switch (action.type) {
    case UPDATE_SELECTED_COMMENT:
      return action.comment !== undefined ? action.comment : null;

    default:
      return state;
  }
}
