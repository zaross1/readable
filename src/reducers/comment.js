import * as actions from "../actions/comment.js";
import { store } from "../store.js";

export function commentReducer(state = store.comments, action) {
  switch (action.type) {
    case actions.CREATE_COMMENT:
      return {
        ...state,
        [action.comment.id]: action.comment
      };

    case actions.UPDATE_COMMENT:
      return {
        ...state,
        [action.comment.id]: action.comment
      };

    case actions.DELETE_COMMENT:
      return {
        ...state,
        [action.id]: {
          ...state[action.id],
          deleted: true
        }
      };

    default:
      return state;
  }
}
