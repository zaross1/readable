import * as actions from "../actions/category.js";
import { store } from "../store.js";

export function categoryReducer(state = store, action) {
  switch (action.type) {
    case actions.CREATE_CATEGORY:
      return {
        ...state,
        categories: state["categories"].concat([action.category])
      };

    case actions.DELETE_CATEGORY:
      return {
        ...state,
        categories: state["categories"].filter(cat => cat !== action.category)
      };

    default:
      return state;
  }
}
