import * as actions from "../actions/post.js";
import { store } from "../store.js";


export function postReducer(state = store.posts, action) {
  switch (action.type) {
    case actions.SET_POSTS:
      return action.posts;

    case actions.CREATE_POST:
      return {
        ...state,
        [action["post"]["id"]]: action.post
      };

    case actions.UPDATE_POST:
      return {
        ...state,
        [action["post"]["id"]]: action.post
      };

    case actions.DELETE_POST:
      return {
        ...state,
        [action["id"]]: {
          ...state[action["id"]],
          deleted: true
        }
      };

    default:
      return state;
  }
}
