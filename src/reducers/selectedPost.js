
import { UPDATE_SELECTED_POST } from "../actions/selectedPost.js";
import { store } from "../store.js";

export function selectedPostReducer(state = store.selectedPost, action) {
  switch (action.type) {
    case UPDATE_SELECTED_POST:
      return action.post;

    default:
      return state;
  }
}
