import React from "react";
import ReactDOM from "react-dom";
import App from "./components/App.js";
import { Provider } from "react-redux";
import thunk from "redux-thunk";
import { createStore, applyMiddleware, compose, combineReducers } from "redux";
import { BrowserRouter } from "react-router-dom";
import registerServiceWorker from "./registerServiceWorker";
import { store as aStore } from "./store.js";
import * as comment_reducers from "./reducers/comment.js";
import * as post_reducers from "./reducers/post.js";
import * as category_reducers from "./reducers/category.js";
import { selectedPostReducer } from "./reducers/selectedPost.js";
import { selectedCommentReducer } from "./reducers/selectedComment.js";
const deepCopy = require("deep-copy");

let myStore = deepCopy(aStore);

const root_reducer = combineReducers({
  comments: comment_reducers.commentReducer,
  categories: category_reducers.categoryReducer,
  posts: post_reducers.postReducer,
  selectedPost: selectedPostReducer,
  selectedComment: selectedCommentReducer
});

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

let startingStore = createStore(
  root_reducer,
  myStore,
  composeEnhancers(applyMiddleware(thunk))
);

ReactDOM.render(
  <Provider store={startingStore}>
    <BrowserRouter>
      <App />
    </BrowserRouter>
  </Provider>,
  document.getElementById("root")
);

registerServiceWorker();
