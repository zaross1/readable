export const updateInfoField = (state, section, id, newValue) => {
  return {
    ...state,
    [section]: {
      ...state[section],
      [id]: newValue
    }
  };
};

export const updateObjProperty = (
  state,
  section,
  id,
  property,
  propertyValue
) => {
  return {
    ...state,
    [section]: {
      ...state[section],
      [id]: {
        ...state[section][id],
        [property]: propertyValue
      }
    }
  };
};

export const arrToObj = arr => {
  let newObj = {};

  for (let i = 0; i < arr.length; i++) {
    newObj[arr[i].id] = arr[i];
  }

  return newObj;
};

export const objToArr = obj => {
  let newArr = [];
  for (let key in obj) {
    newArr.push(obj[key]);
  }
  return newArr;
};

export const setCategories = categories => {
  let newCategories = [];

  for (let i = 0; i < categories.length; i++) {
    //console.log(categories[i])
    newCategories.push(categories[i]["name"]);
  }

  return newCategories;
};

export const createPostDb = (props, post) => {
  fetch("http://localhost:3001/posts", {
    method: "POST",
    headers: {
      Authorization: "something",
      "Content-Type": "application/json"
    },
    body: JSON.stringify(post)
  })
    .then(res => res.json())
    .then(() => props.createPost(post));
};

export const voteDb = (props, body, voteType, url) => {
  fetch(url, {
    method: "POST",
    headers: {
      Authorization: "something",
      "Content-Type": "application/json"
    },
    body: JSON.stringify({option: voteType})  
  })
    .then(res => res.json())
    .then(function(res) {
      props.updatePost !== undefined
        ? props.updatePost(body) && props.changeSelectedPost(body)
        : props.updateComment(body) && props.changeSelectedComment(body);
    });
};

export const updatePostDb = (props, post) => {
  fetch(`http://localhost:3001/posts/${post.id}`, {
    method: "PUT",
    headers: {
      Authorization: "something",
      "Content-Type": "application/json"
    },
    body: JSON.stringify({ body: post.body, title: post.title })
  })
    .then(res => res.json())
    .then(() => props.updatePost(post));
};

export const deletePostDb = (props, postId) => {
  fetch(`http://localhost:3001/posts/${postId}`, {
    method: "DELETE",
    headers: {
      Authorization: "something",
      "Content-Type": "application/json"
    },
    body: JSON.stringify({ id: postId })
  })
    .then(res => res.json())
    .then(function() {
      props.deletePost(postId);

      props.changeSelectedPost(null);
    });
};

export const deleteCommentDb = (props, commentId) => {
  fetch(`http://localhost:3001/comments/${commentId}`, {
    method: "DELETE",
    headers: {
      Authorization: "something",
      "Content-Type": "application/json"
    },
    body: JSON.stringify({ id: commentId })
  })
    .then(res => res.json())
    .then(function() {
      props.deleteComment(commentId);
    });
};

export const createCommentDb = (props, comment) => {
  console.log("create comment db called");

  fetch("http://localhost:3001/comments", {
    method: "POST",
    headers: {
      Authorization: "something",
      "Content-Type": "application/json"
    },
    body: JSON.stringify(comment)
  })
    .then(res => res.json())
    .then(() => props.createComment(comment));
};

export const updateCommentDb = (props, comment) => {
  fetch(`http://localhost:3001/comments/${comment.id}`, {
    method: "PUT",
    headers: {
      Authorization: "something",
      "Content-Type": "application/json"
    },
    body: JSON.stringify({ body: comment.body, title: comment.title })
  })
    .then(res => res.json())
    .then(() => props.updateComment(comment));
};
