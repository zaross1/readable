import * as actions from "../actions/category.js";
import { categoryReducer } from "../reducers/category.js";
import store from "../testStore.js";
const deepCopy = require("deep-copy");

const category1 = "fantasy";
const category2 = "mystery";
const startingStore = deepCopy(store);

describe("basic action creators for the category section of store", () => {
  it("should successfully create a CREATE_CATEGORY action", () => {
    const expectedAction = {
      type: actions.CREATE_CATEGORY,
      category: category1
    };
    const testAction = actions.createCategory(category1);
    expect(testAction).toEqual(expectedAction);
  });

  it("should successfully create a DELETE_CATEGORY action", () => {
    const expectedAction = {
      type: actions.DELETE_CATEGORY,
      category: category1
    };
    const testAction = actions.deleteCategory(category1);
    expect(testAction).toEqual(expectedAction);
  });
});

describe("basic reducer for category section of the store", () => {
  it("should apply a CREATE_CATEGORY action to the store without mutation", () => {
    let testStore = deepCopy(startingStore);
    let expectedStore = deepCopy(startingStore);
    const createAction = actions.createCategory(category2);
    expectedStore["categories"].push(category2);
    expect(categoryReducer(testStore, createAction)).toEqual(expectedStore);
    expect(testStore).toEqual(startingStore);
  });

  it("should apply a DELETE_CATEGORY action to the store without mutation", () => {
    let testStore = deepCopy(startingStore);
    let expectedStore = deepCopy(startingStore);
    const deleteAction = actions.deleteCategory(testStore["categories"][1]);
    expectedStore["categories"].pop();
    expect(categoryReducer(testStore, deleteAction)).toEqual(expectedStore);
  });
});
