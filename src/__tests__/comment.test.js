import * as comment_actions from "../actions/comment.js";
import * as comment_reducers from "../reducers/comment.js";
import store from "../testStore.js";
const deepCopy = require("deep-copy");

const startingStore = deepCopy(store);
// {id, parentId, timestamp, body, author, voteScore, deleted, parentDeleted}

const id = 2;
const parentId = "8xf0y6ziyjabvozdd253nd";
const timestamp = Date.now();
const body = "I am comment 2";
const author = "Jason M";
const voteScore = 0;
const deleted = false;
const parentDeleted = false;
const comment1 = {
  id,
  parentId,
  timestamp,
  body,
  author,
  voteScore,
  deleted,
  parentDeleted
};

describe("testing action creators for Comments section of store", () => {
  it("the CREATE_COMMENT action creator should create a valid comment object", () => {
    const expectedAction = {
      type: comment_actions.CREATE_COMMENT,
      comment: comment1
    };

    const createAction = comment_actions.createComment(expectedAction.comment);
    expect(createAction).toEqual(expectedAction);
  });

  it("the UPDATE_COMMENT action creator should create a valid update comment object", () => {
    const expectedAction = {
      type: comment_actions.UPDATE_COMMENT,
      comment: comment1
    };

    const updateAction = comment_actions.updateComment(expectedAction.comment);
    expect(updateAction).toEqual(expectedAction);
  });

  it("the DELETE_COMMENT action creator should create a valid delete comment object", () => {
    const expectedAction = {
      type: comment_actions.DELETE_COMMENT,
      id
    };

    const deleteAction = comment_actions.deleteComment(expectedAction.id);
    expect(deleteAction).toEqual(expectedAction);
  });
});

describe("Reducer for CRUD comment actions", () => {
  it("CREATE_COMMENT action should successfully add a comment to the store without mutation", () => {
    let testStore = deepCopy(startingStore);
    let expectedStore = deepCopy(startingStore);
    const createAction = comment_actions.createComment(comment1);
    expectedStore["comments"][comment1.id] = comment1;
    expect(comment_reducers.commentReducer(testStore, createAction)).toEqual(
      expectedStore
    );
    expect(testStore).toEqual(startingStore);
  });

  it("UPDATE_COMMENT action should successfully update a comment in the store without mutation", () => {
    let testStore = deepCopy(startingStore);
    testStore["comments"][comment1.id] = comment1;
    let expectedStore = deepCopy(startingStore);
    let updatedComment = Object.assign({}, comment1);
    updatedComment["body"] = "I am an updated comment";
    expectedStore["comments"][comment1.id] = updatedComment;
    const updateAction = comment_actions.updateComment(updatedComment);
    expect(comment_reducers.commentReducer(testStore, updateAction)).toEqual(
      expectedStore
    );
  });

  it("DELETE_COMMENT action should successfully change the 'delete' property of a comment in the store without mutation", () => {
    let testStore = deepCopy(startingStore);
    let expectedStore = deepCopy(startingStore);
    const deleteCommentId = "894tuq4ut84ut8v4t8wun89g";
    expectedStore["comments"][deleteCommentId]["deleted"] = true;
    const deleteAction = comment_actions.deleteComment(deleteCommentId);
    expect(comment_reducers.commentReducer(testStore, deleteAction)).toEqual(
      expectedStore
    );
  });
});
