import * as post_actions from "../actions/post.js";
import * as post_reducers from "../reducers/post.js";
import store from "../testStore.js";
const deepCopy = require("deep-copy");

const title = "Candy";
const body = "This is a candy";
const author = "jason";
const category = "food";
const id = "6ni6ok3ym7mf1p33lnez";
const timestamp = Date.now();
const newTitle = "Happy Days!";
const deleted = false;
const voteScore = 0;
const commentCount = 0;

const startingStore = deepCopy(store);

// testing action creators

describe("action creator: create post", () => {
  it("should create a new post action", () => {
    const expectedAction = {
      type: post_actions.CREATE_POST,
      post: {
        title,
        body,
        author,
        category,
        id,
        timestamp,
        deleted,
        voteScore,
        commentCount
      }
    };

    expect(
      post_actions.createPost({
        id,
        title,
        body,
        author,
        category,
        timestamp,
        deleted: false,
        voteScore: 0,
        commentCount: 0
      })
    ).toEqual(expectedAction);
  });
});

describe("action creator: update post", () => {
  it("should create a new action to update a post", () => {
    const oldPost = store["posts"][id];

    const expectedAction = {
      type: post_actions.UPDATE_POST,
      post: {
        title: newTitle,
        body,
        author: oldPost["author"],
        category: oldPost["category"],
        id,
        timestamp,
        deleted,
        voteScore,
        commentCount
      }
    };

    // {id, title, body, author, category, timeStamp, deleted, voteScore, commentCount}
    expect(
      post_actions.updatePost({
        id,
        title: newTitle,
        body,
        author: oldPost["author"],
        category: oldPost["category"],
        timestamp,
        commentCount,
        voteScore,
        deleted
      })
    ).toEqual(expectedAction);
  });
});

describe("action creator: delete post", () => {
  it("should create a new action to delete a post", () => {
    const expectedAction = {
      type: post_actions.DELETE_POST,
      id
    };
    expect(post_actions.deletePost(id)).toEqual(expectedAction);
  });
});

// testing reducers

describe("reducer: create post", () => {
  it("should apply a create post action to the store", () => {
    let testStore = deepCopy(startingStore);
    let expectedStore = deepCopy(startingStore);
    const createdPost = {
      id: "m",
      timestamp: 5,
      title: "t",
      body: "m",
      author: "j",
      category: "c",
      voteScore: 0,
      deleted: false,
      commentCount: 0
    };

    expectedStore["posts"][createdPost.id] = createdPost;
    expect(
      post_reducers.postReducer(testStore, post_actions.createPost(createdPost))
    ).toEqual(expectedStore);
    console.log(
      post_reducers.postReducer(testStore, post_actions.createPost(createdPost))
    );
    expect(testStore).toEqual(startingStore);
  });
});

describe("reducer: update post", () => {
  it("should apply a update post action to the store", () => {
    let testStore = deepCopy(startingStore);
    let expectedStore = deepCopy(startingStore);
    const updatedPostId = "8xf0y6ziyjabvozdd253nd";
    const updatedTitle = "magic update complete!";
    expectedStore["posts"][updatedPostId]["title"] = updatedTitle;
    const updatedPost = Object.assign(
      {},
      expectedStore["posts"][updatedPostId]
    );
    expect(
      post_reducers.postReducer(testStore, post_actions.updatePost(updatedPost))
    ).toEqual(expectedStore);
    expect(testStore).toEqual(startingStore);
  });
});

describe("reducer: delete post", () => {
  it("should apply a delete post action to the store", () => {
    let testStore = deepCopy(startingStore);
    let expectedStore = deepCopy(startingStore);
    const deletePostId = "8xf0y6ziyjabvozdd253nd";
    expectedStore["posts"][deletePostId]["deleted"] = true;
    expect(
      post_reducers.postReducer(
        testStore,
        post_actions.deletePost(deletePostId)
      )
    ).toEqual(expectedStore);
    expect(testStore).toEqual(startingStore);
  });
});

/*



describe('reducer: delete post', () => {

	it('should apply a delete post action to the store', () => {
		const expectedTestStore1 = {

	posts: {

    '8xf0y6ziyjabvozdd253nd':    {
        id: '8xf0y6ziyjabvozdd253nd',
        timestamp: 1467166872634,
        title: 'Udacity is the best place to learn React',
        body: 'Everyone says so after all.',
        author: 'thingtwo',
        category: 'react',
        voteScore: 6,
        deleted: false,
        commentCount: 2
  }

    },

	comments: {},

	categories: []


}
	
	expect(post_reducers.postReducer(store, post_actions.deletePost({id}))).toEqual(expectedTestStore1)


	})

})


*/
