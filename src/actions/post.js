// user interaction action constants
export const CREATE_POST = "CREATE_POST";
export const DELETE_POST = "DELETE_POST";
export const UPDATE_POST = "UPDATE_POST";
export const SET_POSTS = "SET_POSTS";

export function setPosts(posts) {
  return {
    type: SET_POSTS,
    posts
  };
}

export function createPost({
  id,
  title,
  body,
  author,
  category,
  timestamp,
  deleted,
  voteScore,
  commentCount
}) {
  return {
    type: CREATE_POST,
    post: {
      id,
      title,
      body,
      author,
      category,
      timestamp,
      deleted,
      voteScore,
      commentCount
    }
  };
}

export function updatePost({
  id,
  title,
  body,
  author,
  category,
  timestamp,
  deleted,
  voteScore,
  commentCount
}) {
  return {
    type: UPDATE_POST,
    post: {
      id,
      title,
      body,
      author,
      category,
      timestamp,
      deleted,
      voteScore,
      commentCount
    }
  };
}

export function deletePost(id) {
  return {
    type: DELETE_POST,
    id
  };
}
