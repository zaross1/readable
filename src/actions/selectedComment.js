export const UPDATE_SELECTED_COMMENT = "UPDATE_SELECTED_COMMENT";

export const updateSelectedComment = comment => {
  return {
    type: UPDATE_SELECTED_COMMENT,
    comment
  };
};
