export const CREATE_CATEGORY = "ADD_CATEGORY";
export const DELETE_CATEGORY = "DELETE_CATEGORY";

export function createCategory(category) {
  return {
    type: CREATE_CATEGORY,
    category
  };
}

export function deleteCategory(category) {
  return {
    type: DELETE_CATEGORY,
    category
  };
}
