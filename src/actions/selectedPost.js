export const UPDATE_SELECTED_POST = "UPDATE_SELECTED_POST";

export const updateSelectedPost = post => {
  return {
    type: UPDATE_SELECTED_POST,
    post
  };
};
