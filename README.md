


**General Info**



This app was boot-strapped with create-react-app, and written for the Udacity React Nanodegree program.

Readable is meant to be run concurrently with the Readable back-end server. 




**Installation**

Before performing the installation, make sure the newest version of Node and Yarn are installed
on your computer.

1. in the root directory of Readable, run "yarn install" (you can also use npm install, if you prefer)

**Running Readable**

Since Readable is meant to be used with the Readable back-end, you should fork the readable back-end, run yarn install in that repo, and then run 'node server' to start it before you start Readable.

1. In the root directory of Readable, run "yarn start"

**Testing**

testing platform: Jest

There are some tests for the reducers that can be run via the CLI.

1. Run the following command from the root directory of Readable "yarn test --verbose"


**Formatting**

formatting program / schema: Prettier

after making changes to Readable, in order to ensure consistent styling, run the formatting script: yarn format


